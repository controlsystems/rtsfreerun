# rtsfreerun
Free-running advligorts models in python

[Documentation home page](https://controlsystems.docs.ligo.org/rtsfreerun)

## venv setup
- Install system dependencies, if needed
  - for debian: `apt install git perl build-essential cmake libspdlog-dev rapidjson-dev libjson-perl python3 python3-pip python3-dev python3-venv`
- `python3 -m venv venv && source venv/bin/activate`
- `model=x1sysexample pip install .`
- To build another model (`x1sim` for example), place in `userapps` the `x1sim.mdl` file and any dependencies, then use `model=x1sim pip install .`

## conda setup
- Install system dependencies, if needed
  - for debian: `apt install build-essential`
- `conda create -n rtsfreerun`
- `conda activate rtsfreerun`
- `conda install make cmake spdlog rapidjson pybind11 numpy pyyaml conda-forge::perl-app-cpanminus && cpanm JSON`
- `model=x1sysexample pip install .`
- To build another model (`x1sim` for example), place in `userapps` the `x1sim.mdl` file and any dependencies, then use `model=x1sim pip install .`

## Usage example
```python
import numpy as np
import scipy.signal as sig
import matplotlib.pyplot as plt
import x1sysexample

mdl = x1sysexample.x1sysexample()
duration = 10  # sec

# setting epics variables
mdl.write('SYS-EXAMPLE_CTRL_DAMP_GAIN', 1)
fm = mdl.LIGOFilter('SYS-EXAMPLE_CTRL_DAMP')
fm.turn_off('INPUT')
# loading filters
coef = sig.butter(4, 0.1, btype='low', output='sos', fs=mdl.sample_rate)
mdl.fm_set_sos('EXAMPLE_CTRL_DAMP', 'FM1', coef)
# running excitations
exc = mdl.awg.Sine('SYS-EXAMPLE_CTRL_DAMP_EXC', freq=1, ampl=1)
exc.start()
# acquiring data (runs the model)
buffers = mdl.fetch(0, duration, ['SYS-EXAMPLE_CTRL_DAMP_OUT'])
exc.stop()
plt.plot(buffers[0].data)
plt.show()
```

## Limitations
- Only one model can be imported and run per python process
